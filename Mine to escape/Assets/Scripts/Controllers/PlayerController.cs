﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 3.0f;

    [SerializeField]
    private float distanceToEntity = 2.0f;

    [SerializeField]
    private float rotationSpeed = 240.0f;

    [SerializeField]
    private float speed = 5.0f;

    [SerializeField]
    private float gravity = 20.0f;

    public float P_pickupSpeed = 3.0f;

    private float moveHorizontal;
    private float moveVertical;

    private Vector3 moveDir = Vector3.zero;

    public bool P_isMining = false;

    CharacterController characterController;
    Rigidbody playerRigidbody;
    Inventory playerInventory;
    Animator playerAnimator;

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        characterController = GetComponent<CharacterController>();
        playerInventory = GetComponent<Inventory>();
        playerAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        if (P_isMining)
        {
            moveHorizontal = 0;
            moveVertical = 0;
            playerAnimator.SetBool("IsMining", true);
        }
        else
        {
            moveHorizontal = Input.GetAxisRaw("Horizontal");
            moveVertical = Input.GetAxisRaw("Vertical");
            playerAnimator.SetBool("IsMining", false);
            if (moveVertical != 0 || moveHorizontal != 0)
            {
                playerAnimator.SetBool("IsMoving", true);
            }
            else
            {
                playerAnimator.SetBool("IsMoving", false);
            }
        }

        // Calculate the forward vector
        Vector3 camForward_Dir = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 move = moveVertical * camForward_Dir + moveHorizontal * Camera.main.transform.right;

        if (move.magnitude > 1f) move.Normalize();

        // Calculate the rotation for the player
        move = transform.InverseTransformDirection(move);

        // Get Euler angles
        float _turnAmount = Mathf.Atan2(move.x, move.z);

        transform.Rotate(0, _turnAmount * rotationSpeed * Time.deltaTime, 0);

        if (characterController.isGrounded)
        {
            moveDir = transform.forward * move.magnitude;

            moveDir *= speed;
        }

        moveDir.y -= gravity * Time.deltaTime;

        characterController.Move(moveDir * Time.deltaTime);
    }

    public bool IsCloseToEntity(RaycastHit target)
    {
        Vector3 closestPoint1 = target.collider.ClosestPointOnBounds(transform.position);
        Vector3 closestPoint2 = characterController.ClosestPointOnBounds(target.transform.position);

        float targetDistance = Vector3.Distance(closestPoint1, closestPoint2);

        if (targetDistance <= distanceToEntity)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PickupRessource(RaycastHit ressource)
    {
        if (!P_isMining)
        {
            StartCoroutine(PickupCooldown(ressource));
            P_isMining = true;
        }
    }

    private IEnumerator PickupCooldown(RaycastHit ressource)
    {
        yield return new WaitForSeconds(P_pickupSpeed);
        switch (ressource.collider.tag)
        {
            case "Copper":
                playerInventory.AddRessourceQuantity(0, Random.Range(1, 5));
                break;
            case "Iron":
                playerInventory.AddRessourceQuantity(1, Random.Range(1, 5));
                break;
        }
        P_isMining = false;
        Destroy(ressource.transform.gameObject);
    }

    public void AddRessourceToTarget(RaycastHit target)
    {
        Inventory targetInventory = target.transform.gameObject.GetComponent<Inventory>();
        for (int i = 0; i < targetInventory.GetRessourceTabSize(); i++)
        {
            targetInventory.AddRessourceQuantity(i, playerInventory.GetRessourceQuantity(i));
            playerInventory.SetRessourceQuantity(i, 0);
        }
    }
}
