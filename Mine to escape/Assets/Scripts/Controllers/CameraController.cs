﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform playerTransform;

    [SerializeField]
    [Range(0.01f, 1.0f)]
    private float smoothFactor = 0.5f;

    [SerializeField]
    private float rotationsSpeed = 5.0f;

    private Vector3 cameraOffset;

    private bool lookAtPlayer = false;

    private bool rotateAroundPlayer = true;

    RaycastHit hit;
    private int layer = ~(1 << 13); //13 Corresponds au Layer "Player"

    void Start()
    {
        cameraOffset = transform.position - playerTransform.position;
    }

    void LateUpdate()
    {
        if (rotateAroundPlayer && Input.GetMouseButton(1))
        {
            Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationsSpeed, Vector3.up);
            cameraOffset = camTurnAngle * cameraOffset;
        }

        Vector3 newPos = playerTransform.position + cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);

        if (lookAtPlayer || rotateAroundPlayer)
        {
            transform.LookAt(playerTransform);
        }

        //Fonction permettant à la camera de ne pas passer à travers les objets
        StopCameraClipping();
    }

    void StopCameraClipping()
    {
        if (Physics.Linecast(playerTransform.position, transform.position, out hit, layer)) //On ignore les collisions avec le layer "Player"
        {
            transform.position = hit.point;
        }
    }
}
