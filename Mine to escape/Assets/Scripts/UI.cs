﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField]
    GameManager gameManager;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private GameObject playerBase;

    [SerializeField]
    private GameObject miningCooldown;

    [SerializeField]
    private Text playerCopperInventory;

    [SerializeField]
    private Text playerIronInventory;

    [SerializeField]
    private TextMesh baseCopperInventory;

    [SerializeField]
    private TextMesh baseIronInventory;

    Texture2D cursorTexture;

    [SerializeField]
    Texture2D basicCursorTexture;

    [SerializeField]
    Texture2D pickaxeTexture;

    [SerializeField]
    Texture2D hammerTexture;

    [SerializeField]
    Texture2D baseTexture;

    [SerializeField]
    Texture2D dialogueTexture;

    [SerializeField]
    Material bridgeMaterial;

    CursorMode cursorMode = CursorMode.Auto;
    Vector2 hotSpot = Vector2.zero;

    Color selectcolor;

    PlayerController playerController;
    Inventory playerInventory;
    Inventory baseInventory;
    Slider miningCooldownSlider;

    Ray ray;
    RaycastHit hit;

    void Start()
    {
        playerController = player.GetComponent<PlayerController>();
        playerInventory = player.GetComponent<Inventory>();
        baseInventory = playerBase.GetComponent<Inventory>();
        miningCooldownSlider = miningCooldown.GetComponent<Slider>();
    }

    void Update()
    {
        if (playerController.P_isMining)
        {
            miningCooldown.SetActive(true);
            miningCooldownSlider.value += 1 / playerController.P_pickupSpeed * Time.deltaTime;
        }
        else
        {
            miningCooldown.SetActive(false);
            miningCooldownSlider.value = 0;
        }

        playerCopperInventory.text = "Copper : " + playerInventory.GetRessourceQuantity(0).ToString();
        playerIronInventory.text = "Iron : " + playerInventory.GetRessourceQuantity(1).ToString();

        baseCopperInventory.text = "Copper : " + baseInventory.GetRessourceQuantity(0).ToString();
        baseIronInventory.text = "Iron : " + baseInventory.GetRessourceQuantity(1).ToString();

        Vector2 _mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ressource") ||
                hit.transform.gameObject.layer == LayerMask.NameToLayer("NPC") ||
                hit.transform.gameObject.layer == LayerMask.NameToLayer("Base") ||
                hit.transform.gameObject.layer == LayerMask.NameToLayer("NotBuild"))
            {
                switch (hit.collider.tag)
                {
                    case "Copper":
                    case "Iron":
                        cursorTexture = pickaxeTexture;
                        break;
                    case "Base":
                        cursorTexture = baseTexture;
                        break;
                    case "NPC":
                        cursorTexture = dialogueTexture;
                        break;
                    case "Bridge":
                        cursorTexture = hammerTexture;
                        break;
                }
                OnMouseEnter();
                if (playerController.IsCloseToEntity(hit))
                {
                    //Augmenter la transparence du curseur car proche
                    if (Input.GetMouseButtonDown(1))
                    {
                        switch (hit.transform.gameObject.tag)
                        {
                            case "Copper":
                            case "Iron":
                                playerController.PickupRessource(hit);
                                break;
                            case "NPC":
                                NPC npc = hit.transform.gameObject.GetComponent<NPC>();
                                npc.Dialogue();
                                break;
                            case "Base":
                                playerController.AddRessourceToTarget(hit);
                                break;
                            case "Bridge":
                                hit.collider.isTrigger = false;
                                hit.collider.gameObject.GetComponent<Renderer>().material = bridgeMaterial;
                                hit.collider.gameObject.layer = 12; //Layer Building
                                for (int i = 0; i < 2; i++)
                                {
                                    baseInventory.AddRessourceQuantity(i, -3);
                                }
                                gameManager.ChangeGameState(GameState.Bridge);
                                break;
                        }
                    }
                }
                else
                {
                    //Diminuer la transparence du curseur car trop loin
                }
            }
            else
            {
                OnMouseExit();
            }
        }
        else
        {
            OnMouseExit();
        }
    }

    void OnMouseEnter()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(basicCursorTexture, Vector2.zero, cursorMode);
    }
}