﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    [SerializeField]
    private string npcName;

    [SerializeField]
    private string pnjDialogueText;

    [SerializeField]
    private Text dialogueText;

    [SerializeField]
    private Text nameText;

    [SerializeField]
    private GameObject dialogueView;

    [SerializeField]
    private int rotationSpeed;

    [SerializeField]
    private float speakDistance;

    [SerializeField]
    GameObject player;

    Animator npcAnimator;

    Quaternion startRotation;

    void Start()
    {
        npcAnimator = GetComponent<Animator>();
        startRotation = transform.rotation;
    }

    void Update()
    {
        dialogueView.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);

        if (dialogueView.activeSelf)
        {
            Quaternion LookTarget = Quaternion.LookRotation(player.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, LookTarget, rotationSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, player.transform.position) > speakDistance)
            {
                dialogueView.SetActive(false);
            }
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, startRotation, rotationSpeed * Time.deltaTime);
        }
    }

    public void Dialogue()
    {
        dialogueText.text = pnjDialogueText;
        nameText.text = npcName + " : ";
        npcAnimator.SetTrigger("IsTalking");
        dialogueView.SetActive(true);
    }

    public void SetDialogueText(string text)
    {
        pnjDialogueText = text;
    }
}
