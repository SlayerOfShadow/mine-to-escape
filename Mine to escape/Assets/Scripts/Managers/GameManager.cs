﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private string nameCurrentScene;
    private GameState gameState = GameState.Mine;

    [SerializeField]
    GameObject npc;

    [SerializeField]
    GameObject bridge;

    NPC npcInfo;

    void Awake()
    {
        MakeSingleton();
    }

    void Start()
    {
        npcInfo = npc.GetComponent<NPC>();
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
    }

    public void ChangeGameState(GameState newGameState)
    {
        gameState = newGameState;

        switch (gameState)
        {
            case GameState.Mine:
                break;
            case GameState.Build:
                npcInfo.SetDialogueText("Bravo! Maintenant construit un pont afin d'atteindre l'autre côté");
                bridge.SetActive(true);
                break;
            case GameState.Bridge:
                npcInfo.SetDialogueText("Félicitations! Il ne te reste plus qu'à terminer le niveau");
                break;
            case GameState.Finish:
                print("Vous avez gagné!");
                break;
            default:
                break;
        }
    }

    private void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}

public enum GameState
{
    Mine,
    Build,
    Bridge,
    Finish
}