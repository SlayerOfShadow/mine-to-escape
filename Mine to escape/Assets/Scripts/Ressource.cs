﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ressource
{
    [SerializeField]
    private string ressourceName;

    [SerializeField]
    private int ressourceQuantity;

    public Ressource(string name, int quantity)
    {
        ressourceName = name;
        ressourceQuantity = quantity;
    }

    public int GetQuantity()
    {
        return ressourceQuantity;
    }

    public void SetQuantity(int quantity)
    {
        ressourceQuantity = quantity;
    }

    public void AddQuantity(int amount)
    {
        ressourceQuantity += amount;
    }
}
