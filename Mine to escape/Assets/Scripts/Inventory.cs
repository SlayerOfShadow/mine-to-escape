﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private List<Ressource> ressourceTab = new List<Ressource>();

    public int GetRessourceTabSize()
    {
        return ressourceTab.Count;
    }

    public int GetRessourceQuantity(int index)
    {
        return ressourceTab[index].GetQuantity();
    }

    public void AddRessourceQuantity(int index, int amount)
    {
        ressourceTab[index].AddQuantity(amount);
    }

    public void SetRessourceQuantity(int index, int amount)
    {
        ressourceTab[index].SetQuantity(amount);
    }
}
