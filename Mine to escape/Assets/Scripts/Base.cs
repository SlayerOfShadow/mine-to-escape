﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    [SerializeField]
    GameManager gameManager;

    Inventory baseInventory;

    bool changeState = false;

    void Start()
    {
        baseInventory = GetComponent<Inventory>();
    }

    void Update()
    {
        if (baseInventory.GetRessourceQuantity(0) >= 3 && baseInventory.GetRessourceQuantity(1) >= 3 && !changeState)
        {
            gameManager.ChangeGameState(GameState.Build);
            changeState = true;
        }
    }


}
